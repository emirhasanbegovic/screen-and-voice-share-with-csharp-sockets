﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketSoundVideoStream
{
    class ImageReceiver
    {

        private Socket socket;
        private PictureBox pictureBox;

        public ImageReceiver(Socket socket, PictureBox pictureBox)
        {
            socket.NoDelay = true;
            socket.GetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveBuffer);
            this.pictureBox = pictureBox;
            this.socket = socket;
            Thread thread = new Thread(imageReceiver);
            thread.Start();
        }

        private void imageReceiver()
        {

            while (true)
            {
                byte[] fullImageLengthInBytes = new byte[4];
                int packetSize = socket.Receive(fullImageLengthInBytes, 0, 4, SocketFlags.None);
                int fullImageLength = BitConverter.ToInt32(fullImageLengthInBytes, 0);
               
                byte[] fullImage = new byte[fullImageLength];

                int offset = 0;
                packetSize = socket.Receive(fullImage, 0, fullImageLength, SocketFlags.None);

                offset += packetSize;

                while (offset < fullImageLength)
                {
                    offset += socket.Receive(fullImage, offset, fullImageLength - offset, SocketFlags.None);
                }
                pictureBox.Image = toImg(fullImage);
            }
        }
        

        private Image toImg(byte [] data)
        {
            MemoryStream ms = new MemoryStream(data);
            return Image.FromStream(ms);
        }

    }
}
