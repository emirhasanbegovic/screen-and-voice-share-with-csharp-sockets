﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Media;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SocketSoundVideoStream
{
    class SoundReceiver
    {

        private Socket udpClientSocket;

        private WaveOut waveOut;
        private BufferedWaveProvider bwp;
        private WasapiLoopbackCapture captureInstance;
        
        public SoundReceiver(Socket udpClientSocket)
        {
            this.udpClientSocket = udpClientSocket;
            beginConnection();

            initPlayers();

            Thread soundPlayer = new Thread(soundGetPlay);
            soundPlayer.Start();
        }

        private void initPlayers()
        {
            captureInstance = new WasapiLoopbackCapture();

            bwp = new BufferedWaveProvider(captureInstance.WaveFormat);
            bwp.DiscardOnBufferOverflow = false;
            waveOut = new WaveOut();
    
            waveOut.Init(bwp);
            waveOut.Play();
        }

        private void beginConnection()
        {
            string a = "hello server!";
            udpClientSocket.Send(Encoding.ASCII.GetBytes(a));
        }

        private void soundGetPlay()
        {
            while (true)
            {
                byte[] data = new byte[38400];
                int actualDataLength = udpClientSocket.Receive(data);

                try
                {
                    bwp.AddSamples(data, 0, actualDataLength);
                }
                catch(Exception e)
                {
                    bwp.ClearBuffer();
                }
            }
        }

    }
}
