﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketSoundVideoStream
{

    public partial class Form1 : Form
    {

        public Form1()
        {
           
            InitializeComponent();

            
            DoubleBuffered = true;
            SetStyle(ControlStyles.UserPaint |
              ControlStyles.AllPaintingInWmPaint |
              ControlStyles.ResizeRedraw |
              ControlStyles.ContainerControl |
              ControlStyles.OptimizedDoubleBuffer |
              ControlStyles.SupportsTransparentBackColor
              , true);

            initConnection();

            FormClosed += (s, e) => {
                Debug.WriteLine("application closed");
                Environment.Exit(Environment.ExitCode);
            };

        }
        
        private void initConnection()
        {

            pictureBox1.Width = 1920;
            pictureBox1.Height = 1080;

            //IPHostEntry host = Dns.GetHostEntry("89.212.36.37");
            //IPAddress ipAddress = host.AddressList[0];
            //IPEndPoint remoteEP = new IPEndPoint(ipAddress, 8080);

            Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            sender.Connect(new IPEndPoint(IPAddress.Parse("89.212.36.37"), 8080));

            ImageReceiver imageReceiver = new ImageReceiver(sender, pictureBox1);

            Socket udpClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            udpClientSocket.Connect(new IPEndPoint(IPAddress.Parse("89.212.36.37"), 8081));
            
            SoundReceiver soundReceiver = new SoundReceiver(udpClientSocket);
            
            Socket udpMicClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            udpMicClientSocket.Connect(new IPEndPoint(IPAddress.Parse("89.212.36.37"), 8082));

            MicReceiver micReceiver = new MicReceiver(udpMicClientSocket);

        }

        private void goFullScreen()
        {
            WindowState = FormWindowState.Normal;
            FormBorderStyle = FormBorderStyle.None;
            Bounds = Screen.PrimaryScreen.Bounds;
        }

    }
}
