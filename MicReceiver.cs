﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SocketSoundVideoStream
{
    class MicReceiver
    {
        private WaveOut waveOut;
        private BufferedWaveProvider bwp;
        private Socket udpMicClientSocket;

        public MicReceiver(Socket udpMicClientSocket)
        {
            this.udpMicClientSocket = udpMicClientSocket;

            beginMicConnection();

            Thread soundPlayer = new Thread(soundGetPlay);
            initPlayers();
            soundPlayer.Start();
        }

        private void beginMicConnection()
        {
            string a = "hello MIC server!";
            udpMicClientSocket.Send(Encoding.ASCII.GetBytes(a));
        }

        private void initPlayers()
        {
            bwp = new BufferedWaveProvider(new WaveFormat(84100, WaveIn.GetCapabilities(0).Channels));
            bwp.DiscardOnBufferOverflow = false;
            waveOut = new WaveOut();

            waveOut.Init(bwp);
            waveOut.Play();
        }

        private void soundGetPlay()
        {
            while (true)
            {
                byte[] data = new byte[38400];
                int actualDataLength = udpMicClientSocket.Receive(data);
                //Debug.WriteLine("actualDataLength = "+ actualDataLength);
                try
                {
                    bwp.AddSamples(data, 0, actualDataLength);
                }
                catch (Exception e)
                {
                    bwp.ClearBuffer();
                }
            }
        }

    }
}
